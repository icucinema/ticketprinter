#!/usr/bin/env python
# -*- coding: utf-8 -*-
# 
# Imperial Cinema ticket generator & Printer
#
# An interim script by Adam Ladds
# 
import os, string, serial, ConfigParser, logging, time, datetime

today = datetime.date.today()
logger = logging.getLogger('ticketlog')
hldr = logging.FileHandler('logs/log-' + str(today) + '.log')
formatter = logging.Formatter('%(asctime)s %(message)s')
hldr.setFormatter(formatter)
logger.addHandler(hldr)
logger.setLevel(logging.INFO)
logger.info('Starting up...')

config = ConfigParser.ConfigParser()
config.read("films.cfg")
serial_port = serial.Serial(port='/dev/ttyUSB0', baudrate=19200)

#Clear screen to make it neat-ish
os.system( [ 'clear','cls' ][ os.name == 'nt' ] )

print "Using port", serial_port.portstr

t_limit = 400 # Number of tickets allowed to sell (overall - BOTH films)
film_title_1 = " something " # Initalise value.
film_title_2 = " something "
ticket_template = string.Template('17 JAN   | Imperial Cinema Presents\n\
${member}   | ${film_title_1}\n\
 ${price}   | ${film_title_2} \n\
Imperial | Tuesday 17th January 2013\n\
Cinema   | ${film_time}\n\
${film_time}    | www.imperialinema.co.uk\n\
2305 2030|   ${member}                \n\
 100 ${num} |   ${price}     100 ${num}')
 
 # $member should be 6 chars long (E.g. "Member", "Non-m.", "GSA   ")
 # $price should be 5 chars long (e.g. "10.00", "03.00")
 # $num should be 3 chars long (e.g. "001")

def ConfigSectionMap(section):
    dictl = {}
    options = config.options(section)
    for option in options:
        try:
            dictl[option] = config.get(section, option)
            if dictl[option] == -1:
                DebugPrint("skip: %s" % option)
        except:
           print("exception on %s!" % option)
           dictl[option] = None
    return dictl

def main():
    print "TICKET MAKER v0.6b\n\n"
    member = "-VOID-"
    price = "VOID "
    
    count_1 = 0
    count_2 = 0
    count_double = 0
    
    for i in range(1,t_limit):
        ticket_valid = False
        while not ticket_valid:
            film_number = raw_input("Choose film\nEnter 1 for " + ConfigSectionMap("Film1")['title'] + ",\n Enter 2 for " + ConfigSectionMap("Film2")['title'] +  "\n For double bill, enter d: ")
            if film_number == '1':
                film_title_1 = " "
                film_title_2 = ConfigSectionMap("Film1")['title']
                film_time = ConfigSectionMap("Film1")['time']
                count_1 = count_1 + 1
                num = str(count_1).zfill(3)
                ticket_valid = True
                print_ticket = True
            elif film_number == '2':
                film_title_1 = " "
                film_title_2 = ConfigSectionMap("Film2")['title']
                file_time = ConfigSectionMap("Film2")['time']
                count_2 = count_2 + 1
                num = str(count_2).zfill(3)
                ticket_valid = True
                print_ticket = True
            elif film_number == 'd':
                film_title_1 = ConfigSectionMap("Film1")['title']
                film_title_2 = ConfigSectionMap("Film2")['title']
                film_time = ConfigSectionMap("Film1")['time'] + ' ' + ConfigSectionMap("Film2")['time']
                count_double = count_double + 1
                num = str(count_double).zfill(3)
                ticket_valid = True
                print_ticket = True
            elif film_number == "~":
                film_title_1 = "VOID"
                file_title_2 = "VOID"
                film_time = "VOID"
                num = "000"
                ticket_valid = True
                print_ticket = False  
            else:
                print "Error"
        #current_type = raw_input("Ticket type.\nPress m for member (paid), f for member (first, free), n for non-member:")
        ticket_valid = False
        while not ticket_valid:
            current_type = raw_input("Ticket type.\nPress m for member (paid), f for Member (free, first), n for non-member:")
            if current_type == 'n':
                member = "Non-m."
                if film_number == 'd':
                    price = "07.00"
                else:
                    price = "04.00"
                ticket_valid = True
                drawer_open = True
            elif current_type == 'm':
                member = "Member"
	        if film_number == "d":
                    price = "05.00"
                else:
                    price = "03.00"
                ticket_valid = True
                drawer_open = True
            elif current_type == 'f':
                member = "Member"
                if film_number == "d":
                    price = "02.00"
                    drawer_open = True
                else:
                    price = "Free " #NB Space after word so it's 5 chars. long
                    drawer_open = False
                ticket_valid = True
            elif film_number == '~':
                # No Sale
                member = "-VOID-"
                price = "VOID "
                ticket_valid = True
                drawer_open = True
            else:
                print "Invalid entry, try again"
                ticket_valid = False

        this_ticket = ticket_template.substitute(locals())
        if drawer_open == True:
            # Open cash drawer
            print "\n==========================================\n==Total price:=================== £" + price + " =\n==========================================\n"
	    serial_port.write(chr(0x1B)+chr(0x70)+chr(0x02)+chr(0xFF)+chr(0xFF))
            logger.info('Opening cash drawer.')
        raw_input("\n\nClose drawer (if open) and press ENTER to continue.\n\n")
        if print_ticket:
            # Print ticket
            serial_port.write(this_ticket)
            logger.info('Printing ticket')
            logger.info(this_ticket)
            # Spool and cut
            serial_port.write(chr(0x0C))
            #print "Printing:\n"
            #print this_ticket
        os.system( [ 'clear','cls' ][ os.name == 'nt' ] )
        print "\n\nFilm 1", ConfigSectionMap("Film1")['title'] ," count: ", count_1,"\nFilm 2" , ConfigSectionMap("Film2")['title'] ," count: ", count_2,"\nDouble bill count: ", count_double, "\n"
    
    serial_port.close()
main()
